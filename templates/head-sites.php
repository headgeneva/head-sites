<?php
/**
 * Template pour liste de sites
 *
 *
 * @package headsites
 * Template Name: HEAD Sites
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
		<?php
		while ( have_posts() ) :
			the_post();

			// get_template_part( 'template-parts/content', 'page' );
			
		endwhile; // End of the loop.
		
		
		// On va lister les articles en fonction de trois catégories:
		
		// 1. Sites Institutionnels
		// 2. Filières Master
		// 3. Filières Bachelor et Ateliers
				
		$args = array(
			'post_type' => 'post',
			'category_name' => 'sites-institutionnels',
			'orderby' => 'menu_order',
			'order' => 'ASC'
		);
			
		$custom_query = new WP_Query( $args );
		
		if ( $custom_query->have_posts() ) {
			
			echo '<section class="metagrid">';
			echo '<h2>Site institutionnel</h2>';
			echo '<section class="grid">';
			
			while ( $custom_query->have_posts() ) {
				$custom_query->the_post();
				get_template_part( 'template-parts/content', 'site' );
			}
			echo '</section>';
			echo '</section>';
			wp_reset_postdata();
		}
		
		// 1.bis Sites satellites
		
		$args = array(
			'post_type' => 'post',
			'category_name' => 'sites-satellites',
			'orderby' => 'menu_order',
			'order' => 'ASC'
		);
			
		$custom_query = new WP_Query( $args );
		
		if ( $custom_query->have_posts() ) {
			
			echo '<section class="metagrid">';
			echo '<h2>Ressources internes</h2>';
			echo '<section class="grid">';
			
			while ( $custom_query->have_posts() ) {
				$custom_query->the_post();
				get_template_part( 'template-parts/content', 'site' );
			}
			echo '</section>';
			wp_reset_postdata();
		}
		
		// 2. Filières Bachelor et Master
		
		$args = array(
			'post_type' => 'post',
			'category_name' => 'bachelor-master',
			'orderby' => 'menu_order',
			'order' => 'ASC'
		);
			
		$custom_query = new WP_Query( $args );
		
		if ( $custom_query->have_posts() ) {
			
			echo '<h2>Filières Bachelor et Master</h2>';
			echo '<section class="grid">';
			
			while ( $custom_query->have_posts() ) {
				$custom_query->the_post();
				get_template_part( 'template-parts/content', 'site' );
			}
			echo '</section>';
			wp_reset_postdata();
		}
		
		
		// 3. Ateliers
		
		$args = array(
			'post_type' => 'post',
			'category_name' => 'ateliers',
			'orderby' => 'title',
			'order' => 'ASC'
		);
			
		$custom_query = new WP_Query( $args );
		
		if ( $custom_query->have_posts() ) {
			
			echo '<h2>Ateliers</h2>';
			echo '<section class="grid">';
			
			while ( $custom_query->have_posts() ) {
				$custom_query->the_post();
				get_template_part( 'template-parts/content', 'site' );
			}
			echo '</section>';
			wp_reset_postdata();
		}
		
		// 4. Projets de recherche 
		
		$args = array(
			'post_type' => 'post',
			'category_name' => 'recherche',
			'orderby' => 'title',
			'order' => 'ASC'
		);
			
		$custom_query = new WP_Query( $args );
		
		if ( $custom_query->have_posts() ) {
			
			echo '<h2>Projets de recherche et archives</h2>';
			echo '<section class="grid">';
			
			while ( $custom_query->have_posts() ) {
				$custom_query->the_post();
				get_template_part( 'template-parts/content', 'site' );
			}
			echo '</section>';
			wp_reset_postdata();
		}
		
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
