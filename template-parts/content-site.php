<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package headsites
 */


	$url = get_the_content();
	
	$img = get_the_post_thumbnail_url();
	
	if (!empty($img)) {
	
		$bgstyle = 'style="background-image:url('.$img.')"';
	}

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> <?php echo $bgstyle ?> >
	<?php 

	 ?>
	<a href="<?php echo $url; ?>">
	
	<header class="entry-header">
		<?php the_title( '<h3 class="site-title"><span>', '</span></h3>' ); ?>
	</header><!-- .entry-header -->

	<?php // headsites_post_thumbnail(); ?>

	<div class="entry-content">
		<?php
//		the_content();
		// echo '<p>'. $url .'</p>';
		
		?>
	</div><!-- .entry-content -->
	
	</a>
	<?php ?>
</article><!-- #post-<?php the_ID(); ?> -->
