# HEAD Sites

Thème WordPress pour affichage de la liste des sites hébergés sur head.hesge.ch

## Comment ça fonctionne?

Chaque site est saisi comme un article dans l'interface WordPress.

Chaque article devrait avoir les contenus suivants:

- Le titre: le nom du site.
- Image mise en avant: capture d'écran du site.
- Le champ de contenu: doit contenir l'url toute simple

L'url doit être écrite avec "https://", p.ex. https://head.hesge.ch/biblio/

Elle ne doit pas comporter les balises HTML "a href", elle sera ajoutée par le thème.

### Comment sont groupés les articles?

Par catégories:

1. Sites Institutionnels
2. Filières Master
3. Filières Bachelor et Ateliers

## Infos développeur

Le code principal est dans le modèle de page `templates/head-sites.php`

Le contenu de chaque bloc est dans: `template-parts/content-site.php`

